import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meteo_app/app/data/weather_report.dart';

class HomeController extends GetxController {
  final weatherReportList = Rx<List<WeatherReport>>([]);

  final selectIndex = 0.obs;

  @override
  onInit() {
    super.onInit();
    getWeatherReport();
  }

  getWeatherReport() {
    weatherReportList.value = [
      WeatherReport(
        city: 'Abidjan',
        country: "🇨🇮Côte d'ivoire",
        state: "Suny",
        weatherResult: WeatherResult(
          degree: 28,
          type: WeatherType.sunny,
          weatherAsset: 'assets/cloud_sun.png',
        ),
        height: 23,
        low: 18,
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard ummy text ever since the 1500s",
        subtile: 'subtile',
        colors: [
          Colors.orange.shade400.withOpacity(0.7),
          Colors.orange.withOpacity(0.8),
        ],
        assetBackground: [
          "assets/bg_assets/Clay_Purple0009.png",
          "assets/bg_assets/Clay_Orange0088.png",
        ],
      ),
      WeatherReport(
        city: 'Paris',
        country: "🇫🇷France",
        state: "Suny",
        weatherResult: WeatherResult(
          degree: 15,
          type: WeatherType.cloudy,
          weatherAsset: 'assets/Cloud.png',
        ),
        height: 16,
        low: 10,
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard ummy text ever since the 1500s",
        subtile: 'subtile',
        colors: [
          Colors.grey.shade400.withOpacity(0.7),
          Colors.grey.withOpacity(0.8),
        ],
        assetBackground: [
          "assets/bg_assets/Clay_Purple0009.png",
          "assets/bg_assets/Clay_Orange0088.png",
        ],
      ),
      WeatherReport(
        city: 'Rome',
        country: "🇮🇹Italie",
        state: "rain",
        weatherResult: WeatherResult(
          degree: 12,
          type: WeatherType.rainy,
          weatherAsset: 'assets/Raining.png',
        ),
        height: 15,
        low: 8,
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard ummy text ever since the 1500s",
        subtile: 'subtile',
        colors: [
          Colors.blueGrey.shade400.withOpacity(0.7),
          Colors.blueGrey.withOpacity(0.8),
        ],
        assetBackground: [
          "assets/bg_assets/Fractal_Cube_5_0002.png",
          "assets/bg_assets/Clay_Purple0010.png",
        ],
      ),
      WeatherReport(
        city: 'London',
        country: "🏴󠁧󠁢󠁥󠁮󠁧󠁿England",
        state: "snow",
        weatherResult: WeatherResult(
          degree: 10,
          type: WeatherType.snowy,
          weatherAsset: 'assets/Thunder.png',
        ),
        height: 14,
        low: 6,
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard ummy text ever since the 1500s",
        subtile: 'subtile',
        colors: [
          Colors.black12.withOpacity(0.7),
          Colors.black12.withOpacity(0.8),
        ],
        assetBackground: [
          "assets/bg_assets/Fractal_Cube_5_0001.png",
          "assets/bg_assets/Clay_Purple0010.png",
        ],
      ),
    ];
  }
}
