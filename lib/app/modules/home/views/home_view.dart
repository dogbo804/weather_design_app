import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';

import 'package:get/get.dart';
import 'package:meteo_app/app/components/custom_glassmorphism_widget.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        body: Obx(() {
          return AnimatedContainer(
            duration: const Duration(milliseconds: 500),
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                colors: controller.weatherReportList
                    .value[controller.selectIndex.value].colors!,
              ),
            ),
            child: Stack(
              children: [
                Positioned(
                  left: 10,
                  top: kToolbarHeight,
                  child: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 500),
                    transitionBuilder:
                        (Widget child, Animation<double> animation) {
                      return SlideTransition(
                        position: Tween(
                          begin: const Offset(1, 1),
                          end: Offset.zero,
                        ).animate(animation),
                        child: child,
                      );
                    },
                    child: SizedBox(
                      key: UniqueKey(),
                      height: 300,
                      width: 300,
                      child: Image.asset(
                        controller
                            .weatherReportList
                            .value[controller.selectIndex.value]
                            .assetBackground![0],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: 10,
                  top: kToolbarHeight,
                  child: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 500),
                    transitionBuilder:
                        (Widget child, Animation<double> animation) {
                      return SlideTransition(
                        position: Tween(
                          begin: const Offset(-1, 1),
                          end: Offset.zero,
                        ).animate(animation),
                        child: child,
                      );
                    },
                    child: SizedBox(
                      key: UniqueKey(),
                      height: 300,
                      width: 300,
                      child: Image.asset(controller
                          .weatherReportList
                          .value[controller.selectIndex.value]
                          .assetBackground![1]),
                    ),
                  ),
                ),
                Column(
                  children: [
                    const SizedBox(
                      height: kToolbarHeight,
                    ),
                    Expanded(
                      child: CarouselSlider.builder(
                        onSlideChanged: (value) {
                          controller.selectIndex.value = value;
                        },
                        slideBuilder: (index) {
                          final weatherReport =
                              controller.weatherReportList.value[index];

                          return GlassmorphicContainer(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      weatherReport.city!,
                                      style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        shadows: [
                                          Shadow(
                                            color:
                                                Colors.black.withOpacity(0.3),
                                            offset: const Offset(0.2, 0.2),
                                            blurRadius: 6,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text(
                                      weatherReport.country!,
                                      style: const TextStyle(
                                        fontSize: 13,
                                        color: Colors.white,
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    SizedBox(
                                      // color: Colors.red,
                                      height: 150,
                                      width: 250,
                                      child: Align(
                                        alignment: Alignment.topCenter,
                                        child: Image.asset(
                                          weatherReport
                                              .weatherResult.weatherAsset!,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    Stack(
                                      clipBehavior: Clip.none,
                                      children: [
                                        Text(
                                          '${weatherReport.weatherResult.degree}',
                                          style: TextStyle(
                                            fontSize: 100,
                                            fontWeight: FontWeight.bold,
                                            height: 1,
                                            color: Colors.white,
                                            shadows: [
                                              Shadow(
                                                color: Colors.black
                                                    .withOpacity(0.2),
                                                offset: const Offset(4, 4),
                                                blurRadius: 9,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                          right: -35,
                                          top: -10,
                                          child: Text(
                                            '°',
                                            style: TextStyle(
                                              fontSize: 70,
                                              fontWeight: FontWeight.bold,
                                              height: 1,
                                              color: Colors.white,
                                              shadows: [
                                                Shadow(
                                                  color: Colors.black
                                                      .withOpacity(0.2),
                                                  offset: const Offset(4, 4),
                                                  blurRadius: 9,
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Text(
                                      weatherReport.state!,
                                      style: const TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),
                                    ),
                                    const SizedBox(height: 20),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'High: ${weatherReport.height}',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            color: Colors.white,
                                          ),
                                        ),
                                        const SizedBox(width: 30),
                                        Text(
                                          'Low: ${weatherReport.low}',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ),
                                    const SizedBox(height: 30),
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15),
                                      child: Text(
                                        weatherReport.description!,
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          fontSize: 14,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                  'En savoir plus sur ${weatherReport.city}',
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        slideTransform:
                            const RotateDownTransform(rotationAngle: 5),
                        itemCount: controller.weatherReportList.value.length,
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                      child: Text(
                        'Glisser pour un autre emplacement',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
