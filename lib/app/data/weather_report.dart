import 'package:flutter/material.dart';

class WeatherReport {
  String? city;
  String? country;
  String? state;
  int? height;
  WeatherResult weatherResult;
  int? low;
  String? description;
  String? subtile;
  List<Color>? colors;
  List<String>? assetBackground;

  WeatherReport({
    required this.city,
    required this.country,
    required this.state,
    required this.weatherResult,
    required this.height,
    required this.colors,
    required this.low,
    required this.description,
    required this.subtile,
    required this.assetBackground,
  });
}

class WeatherResult {
  int? degree;
  WeatherType? type;
  String? weatherAsset;

  WeatherResult(
      {required this.degree, required this.type, required this.weatherAsset});
}

enum WeatherType {
  sunny, //ensoillé
  cloudy, // nuageux
  rainy, //pluvieux
  snowy, //neigeux
}
