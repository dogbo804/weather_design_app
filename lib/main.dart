import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:meteo_app/utils/theme_meteo_app.dart';

import 'app/routes/app_pages.dart';

void main() {
  runApp(
    GetMaterialApp(
      title: "Application",
      theme: MeteoAppTheme.lightTheme,
      // darkTheme: NouchiAppTheme.darkTheme,
      initialRoute: AppPages.INITIAL,
      debugShowCheckedModeBanner: false,
      getPages: AppPages.routes,
    ),
  );
}
