import 'package:flutter/material.dart';

class ConstantColors {
  // static const primaryColor = Color(0xFF007AFF);
  static const primaryColor = Colors.orange;

  static const gradiantCustom = [
    primaryColor,
    Color.fromARGB(255, 244, 153, 18),
    Color.fromARGB(255, 241, 166, 52),
    Color.fromARGB(255, 246, 183, 88),
  ];

  static const secondaryColor = Color(0xFF72B6FF);

  static const backgroundDarkColor = Color(0xFF262626);
  static const backgroundLightColor = Colors.white;
}
