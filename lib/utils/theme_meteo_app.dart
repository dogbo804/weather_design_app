// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'constants/constant_colors.dart';

class MeteoAppTheme {
  static final lightTheme = ThemeData.light().copyWith(
    useMaterial3: true,
    listTileTheme: ListTileThemeData(
        textColor: Colors.black,
        style: ListTileStyle.drawer,
        iconColor: Colors.black),
    appBarTheme: AppBarTheme(
      titleSpacing: 0,
      backgroundColor: Colors.transparent,
      foregroundColor: Colors.black,
      elevation: 0,
      titleTextStyle: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontSize: Get.width * 0.05,
        color: Colors.black,
      ),
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: Colors.white,
    ),
    colorScheme: const ColorScheme.light().copyWith(
      primary: ConstantColors.primaryColor,
      onPrimary: Colors.white,
    ),
    inputDecorationTheme: InputDecorationTheme(
      isDense: true, // this will remove the default content padding
      // now you can customize it here or add padding widget
      contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 4),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: ConstantColors.primaryColor,
      foregroundColor: Colors.white,
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: Colors.transparent,
    ),
    radioTheme: RadioThemeData(
      fillColor: MaterialStateProperty.all(ConstantColors.primaryColor),
    ),
    tabBarTheme: TabBarTheme(
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(
          color: ConstantColors.primaryColor,
          width: 2,
        ),
      ),
      labelColor: ConstantColors.primaryColor,
      unselectedLabelColor: Colors.black,
      labelStyle: TextStyle(
        color: ConstantColors.primaryColor,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w700,
        fontSize: 15,
      ),
      unselectedLabelStyle: TextStyle(
        color: ConstantColors.primaryColor,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w400,
        fontSize: 15,
      ),
    ),
    dialogTheme: DialogTheme(
      contentTextStyle: TextStyle(
        color: Colors.black,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w400,
        fontSize: 20,
      ),
      titleTextStyle: TextStyle(
        color: Colors.black,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w700,
        fontSize: 20,
      ),
    ),
    cardTheme: CardTheme(color: Colors.white),
    shadowColor: Colors.grey.withOpacity(0.1),
    textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
      textStyle: MaterialStateProperty.all(
        TextStyle(
          fontFamily: "Plus_Jakarta_Sans",
          fontWeight: FontWeight.w500,
          color: ConstantColors.primaryColor,
          fontSize: 16,
        ),
      ),
    )),
    textTheme: TextTheme(
      displayLarge: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.bold,
        fontSize: 25,
        color: Colors.black,
      ),
      displayMedium: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: Colors.black,
      ),
      titleLarge: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: Colors.black,
      ),
      bodyLarge: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontSize: 13,
        color: Colors.black,
      ),
      bodyMedium: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        color: Colors.black,
      ),
      titleMedium: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        color: Colors.black,
      ),
    ),
  );

  static final darkTheme = ThemeData.dark().copyWith(
    // brightness: Brightness.dark,
    dialogBackgroundColor: ConstantColors.backgroundDarkColor,
    scaffoldBackgroundColor: ConstantColors.backgroundDarkColor,
    listTileTheme: ListTileThemeData(
      textColor: Colors.white,
      style: ListTileStyle.drawer,
      iconColor: Colors.white,
    ),
    drawerTheme:
        DrawerThemeData(backgroundColor: ConstantColors.backgroundDarkColor),
    appBarTheme: AppBarTheme(
      titleSpacing: 0,
      // systemOverlayStyle: SystemUiOverlayStyle.light,
      backgroundColor: ConstantColors.backgroundDarkColor,
      elevation: 0,
      titleTextStyle: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontSize: Get.width * 0.05,
        color: Colors.white,
      ),
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: ConstantColors.backgroundDarkColor,
    ),
    colorScheme: const ColorScheme.light().copyWith(
      primary: ConstantColors.primaryColor,
      onPrimary: Colors.white,
    ),
    inputDecorationTheme: InputDecorationTheme(
      isDense: true, // this will remove the default content padding
      // now you can customize it here or add padding widget
      contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 4),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: ConstantColors.primaryColor,
      foregroundColor: Colors.white,
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: Colors.transparent,
    ),
    radioTheme: RadioThemeData(
      fillColor: MaterialStateProperty.all(ConstantColors.primaryColor),
    ),
    tabBarTheme: TabBarTheme(
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(
          color: ConstantColors.primaryColor,
          width: 2,
        ),
      ),
      labelColor: ConstantColors.primaryColor,
      unselectedLabelColor: Colors.white,
      labelStyle: TextStyle(
        color: ConstantColors.primaryColor,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w700,
        fontSize: 15,
      ),
      unselectedLabelStyle: TextStyle(
        color: ConstantColors.primaryColor,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w400,
        fontSize: 15,
      ),
    ),
    dialogTheme: DialogTheme(
      contentTextStyle: TextStyle(
        color: Colors.white,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w400,
        fontSize: 20,
      ),
      titleTextStyle: TextStyle(
        color: Colors.white,
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.w700,
        fontSize: 20,
      ),
    ),
    cardTheme: CardTheme(color: Colors.white),
    shadowColor: Colors.grey.withOpacity(0.1),
    textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
      textStyle: MaterialStateProperty.all(
        TextStyle(
          fontFamily: "Plus_Jakarta_Sans",
          fontWeight: FontWeight.w500,
          color: ConstantColors.primaryColor,
          fontSize: 16,
        ),
      ),
    )),
    textTheme: TextTheme(
      displayLarge: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.bold,
        fontSize: 25,
        color: Colors.white,
      ),
      displayMedium: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: Colors.white,
      ),
      titleLarge: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: Colors.white,
      ),
      bodyLarge: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        fontSize: 13,
        color: Colors.white,
      ),
      bodyMedium: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        color: Colors.white,
      ),
      titleMedium: TextStyle(
        fontFamily: "Plus_Jakarta_Sans",
        color: Colors.white,
      ),
    ),
  );
}
